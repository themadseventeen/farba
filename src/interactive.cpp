#include "interactive.h"
#include "util.h"
#include "color.h"
#include "options.h"
#include <ncurses.h>
#include <menu.h>
#include <locale.h>
#include <stdio.h>
#include <wchar.h>
#include <list>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>

int xMax, yMax;
WINDOW *sidewin, *mainwin, *titlewin;

void draw_borders() {
	wclear(stdscr);
	for(int i = 1 ; i < LINES - 1 ; i++) {
		mvprintw(i, 0, "\u2502");
		mvprintw(i, COLS - 1, "\u2502");
	}
	for(int i = 1 ; i < COLS - 1 ; i++) {
		mvprintw(0, i, "\u2500");
		mvprintw(LINES - 1, i, "\u2500");
	}
	mvprintw(0, 0, "\u250c");
	mvprintw(LINES - 1, COLS - 1, "\u2518");
	mvprintw(0, COLS - 1, "\u2510");
	mvprintw(LINES - 1, 0, "\u2514");
	mvprintw(0, COLS / OPTION_RATIO, "\u252c");
	mvprintw(LINES - 1, COLS / OPTION_RATIO, "\u2534");
	for(int i = 1 ; i < LINES - 1 ; i++)
		mvprintw(i, COLS / OPTION_RATIO, "\u2502");
}

void handle_resize() {
	draw_borders();
	
}


void interactive() {

	setlocale(LC_ALL, "");
	initscr();
	noecho();
	curs_set(0);
	keypad(stdscr, true);

	sidewin = subwin(stdscr, LINES - 2, COLS / OPTION_RATIO - 1, 1, 1);

	std::vector<Theme *> themes_v = getthemes();
	std::vector<std::vector<Variant *>> variants_v;

	ITEM **themes = new ITEM*[themes_v.size() + 1];
	ITEM ***variants = new ITEM**[themes_v.size()];

	themes[themes_v.size()] = NULL;

	for (int i = 0 ; i < themes_v.size() ; i++) {
		themes[i] = new_item(themes_v.at(i) -> to_string(), "");
		variants_v.push_back(themes_v[i] -> getvariants());
		// set_item_userptr(themes[i], &(variants_v[i]));	
	}

	for (int i = 0 ; i < variants_v.size() ; i++) {
		variants[i] = new ITEM*[variants_v[i].size() + 1];
		variants[i][variants_v[i].size()] = NULL;
		for (int j = 0 ; j < variants_v[i].size() ; j++) {
			variants[i][j] = new_item(variants_v.at(i).at(j) -> to_string(), "");
			set_item_userptr(variants[i][j], variants_v[i][j]);
		}
	}

	MENU *thememenu = new_menu(themes);
	set_menu_mark(thememenu, ">");
	set_menu_fore(thememenu, A_NORMAL);
	draw_borders();

	set_menu_sub(thememenu, sidewin);
	post_menu(thememenu);

	wrefresh(sidewin);

	MENU **varmenus = new MENU*[variants_v.size()];

	for (int i = 0 ; i < variants_v.size() ; i++) {
		varmenus[i] = new_menu(variants[i]);
		set_menu_sub(varmenus[i], sidewin);
	}

	for (int i = 0 ; i < themes_v.size() ; i++) {
		set_item_userptr(themes[i], varmenus[i]);
	}


	int level = 0;
	int key;
	MENU *current_menu = thememenu;
	while(1) {
		key = getch();
		if(key == KEY_RESIZE) {
			handle_resize();
		} else
		if(key == 'q') {
			break;
		}
		if(key == 259) {
			menu_driver(current_menu, REQ_UP_ITEM);
		} else 
		if(key == 258) {
			menu_driver(current_menu, REQ_DOWN_ITEM);
		} else 
		if(key == 10) {
			if(level == 0) {
				level++;
				ITEM *current = current_item(thememenu);
				MENU *tmp = (MENU *)item_userptr(current);
				current_menu = tmp;
				unpost_menu(thememenu);
				post_menu(tmp);
			}
		} else 
		if(key == 260) {
			if(level == 1) {
				level--;
				unpost_menu(current_menu);
				current_menu = thememenu;
				post_menu(current_menu);
			}
		}
	}
	endwin();
	unpost_menu(current_menu);
	free_menu(thememenu);

	for(int i = 0 ; i < themes_v.size() ; i++) {
		free_menu(varmenus[i]);
	}

	delete varmenus;

	for(int i = 0 ; i < themes_v.size() ; i++) {
		free_item(themes[i]);
		for(int j = 0 ; j < variants_v[i].size() ; j++)
			free_item(variants[i][j]);
		delete variants[i];
	}
	delete themes;
	delete variants;
	for(auto x : themes_v) {
		delete x;
	}
	return;
}
