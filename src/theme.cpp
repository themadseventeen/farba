#include <string>
#include <iostream>
#include <cstring>

#include "theme.h"
#include "util.h"

std::vector<Variant *> Theme::getvariants(){
    return variants;
}

const char* Theme::to_string() {
    return name;
}

Theme::Theme(std::filesystem::path path) {
    std::string tmp = getleaf(path);
    int s = tmp.size();
    name = new char[tmp.size() + 1];
    strcpy(name, tmp.c_str());
    for (auto &it : fs::directory_iterator(path)) {
        Variant *tmp = new Variant(it.path());
        variants.emplace_back(tmp);
    }    
}

Theme::~Theme() {
    delete[] name;
    for(auto x : variants)
        delete x;
}