#ifndef THEME_H
#define THEME_H

#include "variant.h"

#include <vector>
#include <filesystem>
#include <string>

class Theme {
	char *name;
	std::vector<Variant *> variants;
public:
	std::vector<Variant *> getvariants();
	const char* to_string();
	Theme(std::filesystem::path);
	~Theme();
};

#endif
