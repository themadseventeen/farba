#ifndef INTERACTIVE_H
#define INTERACTIVE_H

void draw_borders(int, int);

void handle_resize();

void interactive();

#endif