#ifndef COLOR_H
#define COLOR_H

#include <string>

class Color {
public:
    short r, g, b;
    Color(int, int, int);
    Color(std::string);
    std::string to_string();
};

#endif 