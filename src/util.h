#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <list>
#include <utility>
#include <filesystem>
#include <vector>
#include "theme.h"
#include "ncurses.h"

namespace fs = std::filesystem;

/*
* Parses the command line arguments
*/
int parseargs(int, char **);

/*
* Sets global program options based on 
* passed command line agruments
*/
int setprogopts(std::list<std::pair<std::string, std::string>> &);

/*
* Prints the help message to stdout
*/
void print_help();

/*
* Prints a char* visually in the middle of a window
*/
void printcenter(WINDOW *, const char*);

/*
* Find a config and read it
*/
void readconfig();

std::vector<Theme *> getthemes();

fs::path gethomedir();

fs::path getcachedir();

std::string getleaf(fs::path);

std::string padstring(std::string , int, int);

int hextodec(std::string);

int init_extended_color_rgb(int, int, int, int);

int clamp(int, int, int);

#endif