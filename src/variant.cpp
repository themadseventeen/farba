#include "util.h"
#include "variant.h"
#include "color.h"
#include <fstream>
#include <cstring>

namespace fs = std::filesystem;

std::vector<Color> Variant::getcolors() {
    return pallete;
}


void Variant::loadpallete(fs::path variantdir) {
    std::ifstream palletefile(variantdir.string() + "colors");
    std::string color;
    while (palletefile >> color) {
        pallete.emplace_back(Color(color));
    }
}

const char* Variant::to_string () {
    return name;
}

Variant::Variant(fs::path variantdir) {
    std::string tmp = getleaf(variantdir);
    int s = tmp.size();
    name = new char[tmp.size() + 1];
    strcpy(name, tmp.c_str());
    loadpallete(variantdir);
}

Variant::~Variant() {
    delete[] name;
}