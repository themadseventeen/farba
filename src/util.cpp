#include "util.h"
#include "options.h"

#include <string>
#include <iostream>
#include <list>
#include <utility>
#include <cstring>
#include <vector>
#include <filesystem>
#include <unistd.h>
#include <cmath>
#include "ncurses.h"

namespace fs = std::filesystem;

int clamp(int v, int l, int h) {
    if (v < l) return l;
    if (v > h) return h;
    return v;
}

int init_extended_color_rgb(int index, int r, int g, int b) {
    return init_extended_color(index, std::round(std::lerp(0, 999, r / 256.0)),
                                      std::round(std::lerp(0, 999, g / 256.0)),
                                      std::round(std::lerp(0, 999, b / 256.0)));

}

int hextodec(std::string s) {
    int ret;
    ret = (s[1] - isdigit(s[1]) * 48 - !isdigit(s[1]) * 65) * 16 +
          (s[0] - isdigit(s[0]) * 48 - !isdigit(s[0]) * 65) * 256;
    return ret;
}

std::string padstring(std::string x, int pre, int post) {
    std::string ret = x;
    for (int i = 0 ; i < pre ; i++)
        ret = " " + ret;
    for (int i = 0 ; i < post ; i++)
        ret = ret + " ";

    return ret;
}

std::string getleaf(fs::path p) {
    std::string leaf;
    for(auto it = p.begin() ; it != p.end() ; it++) {
        leaf = (*it).string();
    }
    return leaf;
}

fs::path getcachedir() {
    fs::path ret = gethomedir();
    ret += "/.cache/farba";
    return ret;
}

fs::path gethomedir() {
    fs::path ret = "/home/";
    char *usr;
    usr = getlogin();
    ret += fs::path(std::string(usr));
    return ret;
}

std::vector<Theme *> getthemes() {
    fs::path cachedir = getcachedir();
    if(!fs::exists(cachedir)) {
        fs::create_directory(cachedir);
    }
    std::vector<Theme *> themes;
    fs::path themedir = cachedir / "themes";

    if(!fs::exists(themedir)) {
        fs::create_directory(themedir);
    }

    for(auto &it : fs::directory_iterator(themedir)) {
        Theme *tmp = new Theme(it.path());
        themes.push_back(tmp);
    }

    return themes;
}

void printcenter(WINDOW *win, const char *str) {
    int begy, begx, maxy, maxx;
    getbegyx(win, begy, begx);
    getmaxyx(win, maxy, maxx);
    int len = strlen(str);
    mvwprintw(win, maxy / 2, maxx / 2 - len / 2, str);
}

void print_help() {
    std::cout << "Usage: farba [ARGUMENTS]\n" <<
                  "Available arguments :\n" <<
                  "--config=FILE    specifies the config file to use\n";
}

int setprogopts(std::list<std::pair<std::string, std::string>> &options) {

    /*
    *   Default values in case they are not set from the cli
    */

    OPTION_RATIO = 3;

    int interactive = true;
    for(std::list<std::pair<std::string, std::string>>::iterator i = options.begin() ;
        i != options.end() ; i++) {
        std::string key = i -> first, value = i -> second;
        if(key == "--config") {
            OPTION_CONFIG = value;
        } else 
        if (key == "--theme") {
            OPTION_THEME = value;
            interactive = false;
        } else 
        if (key == "--ratio") {
            OPTION_RATIO = std::atoi(value.c_str());
        } else 
        {
            std::cout << "ERROR: Unrecognized argument " << key << "\n";
            print_help();
        }
    }
    return interactive;
}

int parseargs(int argc, char **argv) {
    std::list<std::pair<std::string, std::string>> options;
    for(int i = 1 ; i < argc ; i++) {
        std::string key, value = "";
        int size =  strlen(argv[i]);
        bool kvpair = false;
        int tmp;
        for(int j = 0 ; j < size ; j++) {
            if (argv[i][j] == '=') {
                tmp = j;
                kvpair = 1;
                break;
            }
        }
        if(kvpair){
            key = std::string(argv[i], tmp);
            value = std::string(argv[i] + tmp + 1, size - tmp - 1);
        } else {
            key = std::string(argv[i]);
        }
        options.emplace_back(std::make_pair(key, value));
    }
    return setprogopts(options);
}