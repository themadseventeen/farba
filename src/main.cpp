#include "util.h"
#include "options.h"
#include "interactive.h"

std::filesystem::path OPTION_CONFIG;
std::string           OPTION_THEME;
int                   OPTION_RATIO;
#include <string>
#include <stdio.h>

int main (int argc, char **argv) {
    int mode = parseargs(argc, argv);
    if (mode) {
        // printf("interactive\n");
        interactive();
    }
    // else printf("noninteractive\n");

    return 0;
}