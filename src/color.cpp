#include "color.h"
#include "util.h"

#include <string>

std::string Color::to_string() {
    std::string ret = "";
    return ret + "(" + std::to_string(r) + ", " + std::to_string(g) + ", " + std::to_string(b) + " )";
}

Color::Color(std::string s) {
    if(s.size() != 6)
        return;
    r = hextodec(s.substr(0, 2));
    g = hextodec(s.substr(2, 2));
    b = hextodec(s.substr(4, 2));
}

Color::Color(int r, int g, int b) {
    this -> r = r;
    this -> g = g;
    this -> b = b;
}