#ifndef OPTIONS_H
#define OPTIONS_H

#include <string>
#include <filesystem>


extern int OPTION_RATIO;
extern std::filesystem::path OPTION_CONFIG;
extern std::string OPTION_THEME;

#endif