#ifndef VARIANT_H
#define VARIANT_H

#include "color.h"
#include <vector>
#include <filesystem>
#include <string>

namespace fs = std::filesystem;

class Variant {
public:
    fs::path background;
    Variant(fs::path);
    ~Variant();
    const char* to_string();
    std::vector<Color> getcolors();
private:
    std::vector<Color> pallete;
    char * name;
    void loadpallete(fs::path);
};

#endif